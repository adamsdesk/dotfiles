# Dotfiles

Dotfiles are user-specific configuration files that are typically stored as dotfiles, as in filenames that are prefixed
with a dot or period (.). This project are my personal dotfiles and as such are available for reference and backup.

- [Directory Structure](#directory-structure)
- [Installation](#installation)
  - [Assumptions](#assumptions)
  - [Dependency Requirements](#dependency-requirements)
  - [Setup](#setup)

## Directory Structure

The directory structure listed below is in general terms and only those special
files/directories are listed for explanation purposes.

```console
.
├── bin/            < Executable
├── system/         < System configuration
├── user/           < User configuration
├── CONTRIBUTING.md < Project contributing guidelines
├── LICENSE         < Project source code license
└── readme.md       < Project read me
```

## Installation

### Assumptions

- Have experienced working knowledge within a CLI (command-line interface)
- Understanding of Linux operating system
- Installed all required dependencies as stated in [Dependency Requirements](#dependency-requirements) section
- Installation is done via Linux CLI
- Steps prefixed with a "$" (dollar sign) represents the CLI prompt
- The text after the "$" is to be entered at the CLI
- Setup instructions are an example of installation and configuration

### Dependency Requirements

- BASH v5+
- Git v2+
- GNOME v43+
- GNU Stow v2.3+

### Setup

1. Clone project.
    ```console
    $ git clone https://gitlab.com/adouglas/dotfiles.git
    ```
1. Change to project directory.
    ```console
    $ cd dotfiles
    ```
1. Apply configuration.

   Choose one of the following methods.

    - Specific application
        ```console
		$ cd user/
        $ stow -t ~/ fish
        ```
    - All applications
        ```console
		cd user/
        $ stow -t ~/
        ```
1. Apply GNOME Settings.

    GNOME settings are stored within BASH script that utilizes the command "gsettings" to apply desire settings.
    ```console
    $ ./bin/gsconfig
    ```
