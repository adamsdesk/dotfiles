set -g theme_color_scheme solarized-dark

set -g theme_title_display_path no
set -g theme_title_display_user yes

set -g theme_date_format "+%Y-%m-%d %H:%M:%S - m:%b d:%a w:%V"
set -g theme_display_user yes
set -g theme_display_hostname yes
set -g theme_display_sudo_user yes
set -g theme_show_exit_status yes
set -g theme_display_date yes
set -g theme_display_cmd_duration yes
set -g theme_display_docker_machine yes

set -g theme_display_git_master_branch yes

set -g fish_prompt_pwd_dir_length 0

set -g TERM xterm-256color

neofetch
