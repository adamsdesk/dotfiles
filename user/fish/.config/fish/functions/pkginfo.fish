function pkginfo --description 'package info'
 command pikaur -Qq | fzf --preview 'pikaur -Qil {}' --layout reverse --bind 'enter:execute(pikaur -Qil {} | less)';
end
