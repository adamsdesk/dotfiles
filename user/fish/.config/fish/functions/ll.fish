function ll --description 'alias ll exa -lagF --icons --octal-permissions --git --time-style long-iso'
 command exa -lagF --icons --octal-permissions --group-directories-first --git --time-style long-iso $argv; 
end
