function lt --description 'alias lt exa -lagFT --icons --octal-permissions --git --time-style long-iso'
 command exa -lagFT --icons --octal-permissions --git --time-style long-iso $argv; 
end
