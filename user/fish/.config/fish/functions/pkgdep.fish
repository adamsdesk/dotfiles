function pkgdep --description 'package depedencies'
 command pikaur -Qq | fzf --preview 'pactree -lur {} | sort' --layout reverse --bind 'enter:execute(pactree -lu {} | sort | less)';
end
