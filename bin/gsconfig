#!/usr/bin/env bash
## GNOME Settings > Privacy > File History & Trash

# Nautilus

# file history
gsettings set org.gnome.desktop.privacy remember-recent-files false
# automatically delete trash content
gsettings set org.gnome.desktop.privacy remove-old-trash-files true
# automatically delete temporary files
gsettings set org.gnome.desktop.privacy remove-old-temp-files true
# automatically delete period
gsettings set org.gnome.desktop.privacy old-files-age 1
# default folder view
gsettings set org.gnome.nautilus.preferences default-folder-viewer 'list-view'
# sort folders before files
gsettings set org.gtk.Settings.FileChooser sort-directories-first true
gsettings set org.gnome.nautilus.preferences sort-directories-first true
# sort order
gsettings set org.gtk.Settings.FileChooser sort-column 'name'
gsettings set org.gtk.Settings.FileChooser sort-order 'ascending'
# expandable folders in list view

# list view columns
gsettings set org.gtk.Settings.FileChooser show-size-column true
# Show hidden files
gsettings set org.gtk.Settings.FileChooser show-hidden true
gsettings set org.gnome.nautilus.preferences show-hidden-files true
# default visible columns
gsettings set org.gnome.nautilus.list-view default-visible-columns "['name', 'size', 'owner', 'group', 'permissions', 'date_modified']"
gsettings set org.gnome.nautilus.list-view default-column-order "['name', 'size', 'owner', 'group', 'permissions', 'date_modified']"

# Unknown

# send software usage stats
gsettings set org.gnome.desktop.privacy send-software-usage-stats false
# remember application usage
gsettings set org.gnome.desktop.privacy remember-app-usage false

# GNOME Settings > Power

# blank screen (10 mins)
gsettings set org.gnome.desktop.session idle-delay 600
# automatic suspend (off)
gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-ac-type 'nothing'
# automatic suspend delay
#gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-ac-timeout 1200
# Power button behavior
gsettings set org.gnome.settings-daemon.plugins.power power-button-action 'nothing'

# GNOME Settings > Privacy > Removable Media

# never prompt or autorun/autostart promgras whem media are inserted
gsettings set org.gnome.desktop.media-handling autorun-never true

# GNOME Settings > Privacy > Screen Lock

# blank screen delay
gsettings set org.gnome.desktop.session idle-delay 600
# automatic screen lock
gsettings set org.gnome.desktop.screensaver lock-enabled true
# automatic screen lock delay (seconds)
gsettings set org.gnome.desktop.screensaver lock-delay 300
# show noficiations on lock screen
gsettings set org.gnome.desktop.notifications show-in-lock-screen false

## GNOME Settings > Keyboard > Keyboard Shortcuts > View and Customize Shortcuts
# enable window switching (disables application switching)
# default settings
# gsettings set org.gnome.desktop.wm.keybindings switch-windows '[]'
# gsettings set org.gnome.desktop.wm.keybindings switch-applications "['<Super>Tab', '<Alt>Tab']"
gsettings set org.gnome.desktop.wm.keybindings switch-windows "['<Alt>Tab']"
gsettings set org.gnome.desktop.wm.keybindings switch-applications '[]'

## GNOME Settings > Keyboard > Keyboard Shortcuts > View and Customize Shortcuts > Custom Shortcuts
#https://askubuntu.com/questions/597395/how-to-set-custom-keyboard-shortcuts-from-terminal#597414
# terminal
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ name 'Terminal'
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ binding '<Super>t'
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ command 'kitty'
# nautilus
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/ name 'Nautilus'
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/ binding '<Super>f'
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/ command 'nautilus'
# set available custom shortcuts
gsettings set org.gnome.settings-daemon.plugins.media-keys custom-keybindings "['/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/','/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/','/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2/']"

## GNOME Tweaks > Appearance
# Applications
gsettings set org.gnome.desktop.interface gtk-theme 'Adwaita-dark'
gsettings set org.gnome.desktop.interface color-scheme 'prefer-dark'

## GNOME Tweaks > Top Bar
# show weekday in clock
gsettings set org.gnome.desktop.interface clock-show-weekday true
# weather the clock show seconds
gsettings set org.gnome.desktop.interface clock-show-seconds true
# show date in clock
gsettings set org.gnome.desktop.interface clock-show-date true
# weather the clock displays  24h or 12h format
gsettings set org.gnome.desktop.interface clock-format '24h'
# show week number
gsettings set org.gnome.desktop.calendar show-weekdate true

## GNOME Tweaks > Window Titlebars
gsettings get org.gnome.desktop.wm.preferences action-middle-click-titlebar 'minimize'

## GNOME Tweaks > Workspaces
# Display Handling, wrorkspaces span displays (false)
gsettings set org.gnome.mutter workspaces-only-on-primary false

## GNOME Settings > Mouse & Touchpad
# mouse speed
gsettings set org.gnome.desktop.peripherals.mouse speed 1.0
# natural scrolling
gsettings set org.gnome.desktop.peripherals.mouse natural-scrol true

## gedit
gsettings set org.gnome.gedit.preferences.editor tabs-size uint32 4
gsettings set org.gnome.gedit.preferences.editor scheme 'oblivion'
gsettings set org.gnome.gedit.preferences.editor editor-font 'Monospace 12'
gsettings set org.gnome.gedit.preferences.editor syntax-highlighting true
gsettings set org.gnome.gedit.plugins.spell highlight-misspelled true
gsettings set org.gnome.gedit.preferences.editor insert-spaces true
gsettings set org.gnome.gedit.preferences.editor display-line-numbers true

## GNOME Settings > Displays
# scale
gsettings set org.gnome.desktop.interface scaling-factor 2

## GNOME Clocks **MUST CONFIRM**
gsettings set org.gnome.clocks world-clocks [{'location': <(uint32 2, <('Toronto', 'CYTZ', true, [(0.76154532446909495, -1.3857914260834978)], [(0.76212711252195475, -1.3860823201099277)])>)>}]
gsettings set org.gnome.shell.world-clocks locations [<(uint32 2, <('Toronto', 'CYTZ', true, [(0.76154532446909495, -1.3857914260834978)], [(0.76212711252195475, -1.3860823201099277)])>)>]

## GNOME Settings > Default Applications
# set via ~/.config/mimeapps.list

# these seem to be unrelated to GNOME Settings > Default Applications
# gsettings set org.gnome.desktop.default-applications.terminal exec-arg '-x'
# gsettings set org.gnome.desktop.default-applications.terminal exec 'gnome-terminal'
# gsettings set org.gnome.desktop.default-applications.office.calendar exec 'evolution -c calendar'
# gsettings set org.gnome.desktop.default-applications.office.calendar needs-term false
# gsettings set org.gnome.desktop.default-applications.office.tasks exec 'evolution -c tasks'
# gsettings set org.gnome.desktop.default-applications.office.tasks needs-term false
